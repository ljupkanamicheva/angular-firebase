// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAzkAznYuarUKG0EWe8_fV_X_eQ8Wjs0XM",
    authDomain: "angular-firebase-22.firebaseapp.com",
    databaseURL: "https://angular-firebase-22.firebaseio.com",
    projectId: "angular-firebase-22",
    storageBucket: "angular-firebase-22.appspot.com",
    messagingSenderId: "855251974716"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
